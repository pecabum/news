import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/home/home';
import DetailsScreen from './src/screens/details/details';

const AppNavigator = createStackNavigator(
	{
		Home: HomeScreen,
		Details: DetailsScreen
	},
	{
		initialRouteName: 'Home'
	}
);

export default createAppContainer(AppNavigator);
