import React, { PureComponent, useState, useEffect } from 'react';
import { Text, ActivityIndicator, Dimensions, StyleSheet, FlatList } from 'react-native';
import ListItem from '../../customComponents/ListItem';
import { NEWS_URL } from '../../config';

export default (HomeScreen = (props) => {
	const [ news, setNews ] = useState([]);
	const [ refreshing, setRefreshing ] = useState(false);

	useEffect(() => {
		fetchData();
	}, []);

	function fetchData() {
		fetch(NEWS_URL)
			.then((res) => res.json())
			.then((res) => {
				console.log(res.articles);
				setNews(res.articles);
				setRefreshing(false);
			})
			.catch((err) => {
				console.log('Error fetching data');
			});
	}

	if (news.length > 0) {
		return (
			<FlatList
				style={{ flex: 1 }}
				contentContainerStyle={{ alignSelf: 'stretch' }}
				data={news}
				onRefresh={() => {
					fetchData();
				}}
				keyExtractor={(item) => item.publishedAt}
				refreshing={refreshing}
				renderItem={(data) => {
					console.log(data);
					return (
						<ListItem
							data={data}
							onPress={() => {
								props.navigation.navigate('Details');
							}}
						/>
					);
				}}
			/>
		);
	} else {
		return <ActivityIndicator />;
	}
});

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 22
	},
	ContainerView: {
		// backgroundColor:'grey',
		marginBottom: 20,
		paddingVertical: 10,
		backgroundColor: '#F5F5F5',

		borderBottomWidth: 0.5,
		borderBottomColor: 'grey',
		width: deviceWidth - 40,
		marginLeft: 20,
		marginRight: 20,
		marginTop: 20,
		flexDirection: 'row'
	}
});
