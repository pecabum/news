import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';

export default (ListItem = (props) => {
	return (
		<TouchableOpacity onPress={props.onPress}>
			<Image
				style={{ flex: 1, width: '100%', height: 200 }}
				source={{ uri: props.data.item.urlToImage }}
			/>
			<Text style={{ fontSize: 20, color: '#000' }}>{props.data.item.title}</Text>
		</TouchableOpacity>
	);
});
